import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input-mobile-no',
  templateUrl: './input-mobile-no.component.html',
  styleUrls: ['./input-mobile-no.component.scss']
})
export class InputMobileNoComponent implements OnInit {
  @Output() newItemEvent = new EventEmitter<number>();
  page: number = 1;
  mobileNo: string;
  constructor() { }

  ngOnInit(): void {
    
  }

  onSubmit(){
    this.newItemEvent.emit(this.page++);
  }
}
