import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // pageTitle = "ใส่บัตรประชาชน";
  pageTitle = "แสดงตัวตน";
  locationCode = 1100;
  ascCode = "4567";

  step = 1;

  onChangeTitle(type) {
    if(type===1) {
      this.pageTitle = "แสดงตัวตน";
    } else if(type===2) {
      this.pageTitle = "ใส่บัตรประชาชน";
    }
  }

  setPage(page: number){
    this.step = page;
  }
}
