import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputMobileNoComponent } from './input-mobile-no.component';

describe('InputMobileNoComponent', () => {
  let component: InputMobileNoComponent;
  let fixture: ComponentFixture<InputMobileNoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputMobileNoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputMobileNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
