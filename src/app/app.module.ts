import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderBarComponent } from './components/header-bar/header-bar.component';
import { TitleBarComponent } from './components/title-bar/title-bar.component';
import { InfoBarComponent } from './components/info-bar/info-bar.component';
import { InputMobileNoComponent } from './components/input-mobile-no/input-mobile-no.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderBarComponent,
    TitleBarComponent,
    InfoBarComponent,
    InputMobileNoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
